module.exports = {
  season: 'division.bro.official.pc-2018-03',
  roles: [
    { score: -0.1, role: 'retard goblin', emoji: '👺' },
    { score: 1.0, role: 'mediocre', emoji: '🥥' },
    { score: 1.5, role: 'exceeds expectations', emoji: '🦶' },
    { score: 2, role: 'actually not bad', emoji: '⛳' },
  ],
  unverifiedRole: 'unverified scum',
  permRole: 'captain',
  permIDS: ['117691721726427145a'],
};
