const axios = require('axios');
const Discord = require('discord.js');
const _ = require('lodash');
const {
  roles, season, unverifiedRole, permRole, permIDS,
} = require('./config');
const tk = require('./token');
const animals = require('./animals');
const adjectives = require('./adjectives');

const roleNames = _.map(roles, role => role.role);
const client = new Discord.Client();


client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});


client.login(tk.discord);

const axiosIns = axios.create({
  headers: {
    Authorization: tk.pubg,
    accept: 'application/vnd.api+json',
  },
});

function API(endpoint, query) {
  return axiosIns.get(`https://api.pubg.com/shards/steam/${endpoint}?${query}`);
}

function animalInsult() {
  const adj = adjectives[Math.floor(Math.random() * adjectives.length)];
  const animal = animals[Math.floor(Math.random() * animals.length)];
  return `${adj} ${animal}`;
}
// TODO: update season automatically
// API('seasons').then((resp) => {
//   // eslint-disable-next-line
//   debugger
//   console.log(resp.data);
// });

const verify = (msg) => {
  // first, check the user has permission to use this command
  const hasPermRole = msg.member.roles.some(role => role.name === permRole);
  const hasPermID = permIDS.indexOf(msg.member.id) !== -1;
  if (!(hasPermRole || hasPermID)) {
    msg.channel.send(`You don't have permission to use this command, you ${animalInsult()} 😼`);
    return;
  }
  console.log(hasPermRole);
  console.log('Verifying::', msg.content);
  const [, pubgPlayer] = msg.content.split(' ');
  if (pubgPlayer === '-h' || pubgPlayer === '--help' || typeof pubgPlayer === 'undefined' || pubgPlayer === 'help') {
    msg.channel.send('```PUBG verification bot\nUsage: !verify [pubg-name] @discord-user\nAssigns a discord user roles based off ther KA/L ratio for the season in Squad FPP mode.```');
    return;
  }
  const member = msg.mentions.members.first();
  if (typeof member === 'undefined') {
    msg.channel.send(`Gotta @mention a discord user ya ${animalInsult()} 😒`);
    return;
  }
  // find pubgPlayer details
  API('players', `filter[playerNames]=${pubgPlayer}`).then((resp) => {
    API(`players/${resp.data.data[0].id}/seasons/${season}`).then((seasonResults) => {
      const seasonStats = seasonResults.data.data.attributes.gameModeStats['squad-fpp'];
      if (seasonStats.roundsPlayed === 0) {
        msg.channel.send(`Player ${pubgPlayer} hasn't played any Squad FPP games this season. So I can't rank them. 😟`);
        return;
      }
      const KA = seasonStats.assists + seasonStats.kills;
      let L = seasonStats.losses;
      if (L === 0) {
        L = 1;
      }
      const KAL = KA / L;
      const KALR = Math.round(KAL * 1000) / 1000;
      let outInd = roles.length - 1;
      roles.some((r, ind) => {
        if (r.score > KAL) {
          outInd = (ind - 1);
          return true;
        }
        return false;
      });
      // so, we want to get their current roles as an array
      // remove any mentioned roles
      // and add the relevant one
      const newRoles = [];
      member.roles.forEach((role) => {
        if (roleNames.indexOf(role.name) === -1 && role.name !== unverifiedRole) {
          newRoles.push(role);
        }
      });
      newRoles.push(msg.guild.roles.find(role => role.name === roles[outInd].role));
      member.setRoles(newRoles);
      msg.channel.send(`Player '${pubgPlayer}' has played ${seasonStats.roundsPlayed} rounds this season, and has a KA/L of ${KALR}\nThis assigns them role '${roles[outInd].role}' ${roles[outInd].emoji}`);
    }).catch((err) => {
      throw err;
    });
  }).catch((err) => {
    switch (err.response.status) {
      case 429:
        msg.channel.send(`PUBG API has rate limited me, please try again in one minute. 🙄`);
        break;
      case 404:
        msg.channel.send(`Player '${pubgPlayer}' not found in PUBG API 😞`);
        break;
      default:
        msg.channel.send(`UwU unknown ewwow encowntewed, senpai I'm scareded`);
        console.error(`Unknown error : ${err.response.status} (${err})`, err);
    }
  });
};

const link = (msg) => {
  const targetPlayer = msg.content.split(' ').slice(1).join(' ');
  msg.channel.send(`https://pubg.op.gg/user/${targetPlayer}`);
};

client.on('message', (msg) => {
  if (_.startsWith(msg.content, '!verify')) {
    verify(msg);
  }
  if (_.startsWith(msg.content, '!link')) {
    link(msg);
  }
});


client.on('guildMemberAdd', (member) => {
  member.addRole(member.guild.roles.find(role => role.name === unverifiedRole));
  console.log('Member added');
});
